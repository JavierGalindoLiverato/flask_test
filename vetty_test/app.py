import os.path
from flask import Flask, request, Response
import itertools
import json

ALLOWED_EXTENSIONS = set(['txt'])
ROOT_DIR = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

def describe_file(filename, start_line, end_line):
    file = get_file(filename, start_line, end_line)
    if (file == None) :
        return Response(json.dumps({
            'message': 'File not found',
            'success': False
        }), mimetype=u'application/json', status=404) 
    num_lines = len(file)
    if allowed_file(filename):
        return Response(json.dumps({
            'file_read': filename,
            'number_of_lines_read': num_lines,
            'lines': file,
            'success': True
        }), mimetype=u'application/json', status=200) 
    else:
        return Response(json.dumps({
            'message': 'File extension not supported',
            'success': False
        }), mimetype=u'application/json', status=422) 

app.add_url_rule('/describe_file/', 'describe_file', describe_file, defaults={'filename': 'file1.txt', 'start_line': 0, 'end_line': None}, methods=['GET'])  
app.add_url_rule('/describe_file/<filename>/', 'describe_file', describe_file, defaults={'start_line': 0, 'end_line': None}, methods=['GET'])
app.add_url_rule('/describe_file/<filename>/<int:start_line>/', 'describe_file', describe_file, defaults={'end_line': None}, methods=['GET'])
app.add_url_rule('/describe_file/<int:start_line>/', 'describe_file', describe_file, defaults={'filename': 'file1.txt', 'end_line': None}, methods=['GET'])
app.add_url_rule('/describe_file/<int:start_line>/<int:end_line>/', 'describe_file', describe_file, defaults={'filename': 'file1.txt'}, methods=['GET'])
app.add_url_rule('/describe_file/<filename>/<int:start_line>/<int:end_line>/', 'describe_file', describe_file, methods=['GET'])

def get_file(filename, start_line, end_line):
    try:
        file = open(os.path.join(ROOT_DIR, filename))
        list = []
        for line in itertools.islice(file, start_line, end_line):
            list.append(line)
        return list
    except IOError as e:
        return None


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


if __name__ == '__main__':
    app.run()